﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TemperatureConverter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void txtInputTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TempConvert();
        }

        private void rbtnInputCelsius_Checked(object sender, RoutedEventArgs e)
        {
            TempConvert();
        }

        public void TempConvert()
        {
            double temp;
            if (!Double.TryParse(txtInputTextBox.Text, out temp))
            {
                return;
            }

            if (rbtnInputCelsius.IsChecked == true)
            {
                if (rbtnOutputCelsius.IsChecked == true)
                {
                    result.Content = string.Format("{0:0.0}", temp);
                }
                else if (rbtnOutputFarenheit.IsChecked == true)
                {
                    result.Content = string.Format("{0:0.0}", (temp*1.8+32));
                }
                else if (rbtnOutputKelvin.IsChecked == true)
                {
                    result.Content = string.Format("{0:0.0}", (temp+273.75));
                }
            }

            if (rbtnInputFarenheit.IsChecked == true)
            {
                if (rbtnOutputCelsius.IsChecked == true)
                {
                    result.Content = string.Format("{0:0.0}", ((temp-32)/1.8 + 32));
                }
                else if (rbtnOutputFarenheit.IsChecked == true)
                {
                    result.Content = string.Format("{0:0.0}", temp);
                }
                else if (rbtnOutputKelvin.IsChecked == true)
                {
                    result.Content = string.Format("{0:0.0}", ((temp - 32)/1.8 +273.15));
                }
            }

            if (rbtnInputKelvin.IsChecked == true)
            {
                if (rbtnOutputCelsius.IsChecked == true)
                {
                    result.Content = string.Format("{0:0.0}", temp-273.15);
                }
                else if (rbtnOutputFarenheit.IsChecked == true)
                {
                    result.Content = string.Format("{0:0.0}", ((temp - 273.15)*1.8 +32));
                }
                else if (rbtnOutputKelvin.IsChecked == true)
                {
                    result.Content = string.Format("{0:0.0}", temp);
                }
            }
        }
    }
}
